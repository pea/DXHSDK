DXHSDK 是一个基于iOS 7.0及以上版本的导学号应用程序开发接口，供开发者在自己的iOS应用中加入导学号相关功能，通过DXHSDK，开发者可以开发出根据给定导学号如“001005”，显示辅导信息，并且可以通过对题目拍照，搜索相应辅导信息的功能。

## 安装

### 通过CocoaPods

如果初次使用CocoaPods,请参考[使用指南](http://code4app.com/article/cocoapods-install-usage)http://code4app.com/article/cocoapods-install-usage

在工程目录下

#### 1. 创建Podfile:

```bash
$ touch Podfile
```
#### 2. 编辑Podfile内容如下:

```ruby
pod 'DXHSDK','~>1.0.0'         //需要指定版本号
```

#### 3. 安装

```bash
$ pod install
```

#### 4. 注意问题
如果出现头文件找不到

<p align="center" >
<img src="http://lftcdn.52fdw.com/lftcdn/img/sdk/headerNotFound.png" width="80%">
</p>

请到Building Setting -> User Header Search Paths 中作如下设置：
<p align="center" >
<img src="http://lftcdn.52fdw.com/lftcdn/img/sdk/userHeaderSearchPaths.png" width="80%">
</p>

### 手动导入
#### 1. 将下载的DXHSDK.vxx.zip解包，导入到工程目录下:

<p align="center" >
<img src="http://lftcdn.52fdw.com/lftcdn/img/sdk/addSdk.png" width="80%">
</p>

另外，还需要导入'AFNetworking','~>3.1', 'CocoaSecurity','~>1.2', 'LLSimpleCamera','~>4.2', 'MBProgressHUD', 'MJExtension','~>3.0', 'TOCropViewController','~>1.3', 'NYXImagesKit' 

项目需要依赖系统库  libc++.tbd


[点击下载](http://lftcdn.52fdw.com/lftcdn/sdk/DXHSDK.v0.0.7.zip)
#### 2. 配置building

<p align="center" >
<img src="http://lftcdn.52fdw.com/lftcdn/img/sdk/searchPath0.0.5.png" width="80%">
</p>

<p align="center" >
<img src="http://lftcdn.52fdw.com/lftcdn/img/sdk/allLoad.png" width="80%">
</p>


## 使用

* <h3>配置key和token</h3>

```objective-c
[DXHServices sharedServices].appKey = @"appKey";
[DXHServices sharedServices].token = @"token";
```
appKey和token是使用iOS DXHSDK的身份验证，在[导学号开发者中心](http://developer.daoxuehao.com/app)申请和查看
<p align="center" >
<img src="http://lftcdn.52fdw.com/lftcdn/img/sdk/appKey.png" width="80%">
</p>

* <h3>配置文件</h3>
为保证SDK的功能在iOS 9中正常使用，需在“Info.plist”中做如下配置，
Xcode9.0之后版本，键入NSAppTransportSecurity字段会自动调整为App Transport Security Settings：

<p align="center" >
<img src="http://lftcdn.52fdw.com/lftcdn/img/sdk/info.png" width="80%">
</p>

* <h3>根据导学号显示辅导信息页</h3>

ViewController.m 文件中

```objective-c
-(void)viewDidLoad{
[super viewDidLoad];
UIStoryboard *dxhsb = [UIStoryboard storyboardWithName:@"DXHSDK" bundle: [NSBundle bundleWithIdentifier:@"DXHSDK.bundle"]];
DXHNavController *nvc = [dxhsb instantiateInitialViewController];
[self.navigationController pushViewController:nvc.viewControllers.firstObject animated:YES];

}
```


需要自己定制入口页面的，可以参考如下三个类QuestionDetailsViewController，CameraViewController，SearchViewController的接口使用，分别对应
使用导学号查题，拍照，使用图片搜题三个功能

* <h3>使用导学号查题类QuestionDetailViewController</h3>

ViewController.m 文件中

```objective-c
-(void)viewDidLoad{
[super viewDidLoad];
UIStoryboard *dxhsb = [UIStoryboard storyboardWithName:@"DXHSDK" bundle: [NSBundle bundleWithIdentifier:@"DXHSDK.bundle"]];
QuestionDetailsViewController *vc = [dxhsb instantiateViewControllerWithIdentifier:@"QuestionDetailsViewController"];
vc.dxh = @"001005";
[self presentViewController:vc animated:YES completion:nil];
}
```

* <h3>使用导学号拍照类CameraViewController</h3>

首先，需要遵守CameraViewDelegate代理协议
ViewController.h 文件

```objective-c
#import <UIKit/UIKit.h>
#import "CameraViewController.h"

@interface ViewController : UIViewController<CameraViewDelegate>

@end
```

接着，初始化CameraViewController，设置代理
ViewController.m 文件中

```objective-c
-(void)viewDidLoad{
[super viewDidLoad];
UIStoryboard *dxhsb = [UIStoryboard storyboardWithName:@"DXHSDK" bundle: [NSBundle bundleWithIdentifier:@"DXHSDK.bundle"]];
CameraViewController *vc = [[CameraViewController alloc] init];
vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
vc.modalPresentationStyle = UIModalPresentationFullScreen;
vc.delegate = self;
[self presentViewController:vc animated:YES completion:nil];
}

```

最后，实现代理方法，参数image为拍照后截取的图片，SearchViewController图片搜索后返回的结果
ViewController.m 文件中

```objective-c
- (void)takeCameraImage:(UIImage *)image {
UIStoryboard *dxhsb = [UIStoryboard storyboardWithName:@"DXHSDK" bundle: [NSBundle bundleWithIdentifier:@"DXHSDK.bundle"]];
SearchViewController *svc = [dxhsb instantiateViewControllerWithIdentifier:@"SearchViewController"];
svc.sourceImage = image;
[self.navigationController pushViewController:svc animated:YES];
}
```
* <h3>使用导学号扫一扫类ScanViewController</h3>

首先，需要遵守ScanDelegate代理协议
ViewController.h 文件

```objective-c
#import <UIKit/UIKit.h>
#import "ScanViewController.h"

@interface ViewController : UIViewController<ScanDelegate>

@end
```

接着，初始化ScanViewController，设置代理
ViewController.m 文件中

```objective-c
-(void)viewDidLoad{
[super viewDidLoad];
UIStoryboard *dxhsb = [UIStoryboard storyboardWithName:@"DXHSDK" bundle: [NSBundle bundleWithIdentifier:@"DXHSDK.bundle"]];
ScanViewController *vc = [dxhsb instantiateViewControllerWithIdentifier:@"DXHScanViewController"];
vc.delegate = self;
[self presentViewController:vc animated:YES completion:nil];
}

```

最后，实现代理方法，参数number为扫描到的导学号，ScanViewController扫描后返回的结果
ViewController.m 文件中

```objective-c
-(void)numberByScan:(NSString *)number{
    UIStoryboard *dxhsb = [UIStoryboard storyboardWithName:@"DXHSDK" bundle: [NSBundle bundleWithIdentifier:@"DXHSDK.bundle"]];
    QuestionDetailsViewController *vc = [dxhsb instantiateViewControllerWithIdentifier:@"QuestionDetailsViewController"];
    vc.dxh = number;
    [self.navigationController pushViewController:vc animated:YES];
}
```
* <h3>“获取答案”按钮点击事件</h3>
如果要对“获取答案”按钮的显示进行控制，只需要在ViewController观察类中注册通知，并实现通知的调用方法：

* <h6>注册通知</h6>
```objective-c
// 注册通知，分别为输入导学号查题页面点击“获取答案”，拍照搜索页面点击“获取答案”的通知注册
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTappedShowAnswerButtonInQuestionDetailsView:) name:@"getAnswerButtonTappedInQuestionDetailsView" object:nil];
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTappedShowAnswerButtonInSearchView:) name:@"getAnswerButtonTappedInSearchView" object:nil];
```

* <h6>通知方法实现</h6>
```objective-c
// 通知方法实现
//输入导学号查题页面点击"获取答案"发出通知，调用的方法
-(void)handleTappedShowAnswerButtonInQuestionDetailsView:(NSNotification *) notification{
    QuestionDetailsViewController *qdvc = notification.object;
    // 显示答案方法
    [qdvc openAnswer];
}
```

```objective-c  
//拍照搜索页面点击"获取答案"发出通知，调用的方法
-(void)handleTappedShowAnswerButtonInSearchView:(NSNotification *) notification{
    SearchViewController *svc = notification.object;
    //显示答案方法
    [svc openAnswer];
}
```

下面举例说明如何设置pageType，控制“获取答案”按钮的显示

###### 1 首页MainViewController显示"获取答案"按钮
ViewController.m 文件中

```objective-c
    DXHNavController *nvc = [dxhsb instantiateInitialViewController];
    MainViewController *mvc = nvc.viewControllers.firstObject;
    // pageType初始化
    mvc.pageType = GET_ANSWER_BUTTON_SHOWN;
    [self.navigationController pushViewController:mvc animated:YES];
```

###### 2 导学号查题类QuestionDetailViewController 显示"获取答案"按钮

```objective-c    
    QuestionDetailsViewController *qdvc = [dxhsb instantiateViewControllerWithIdentifier:@"QuestionDetailsViewController"];
    qdvc.pageType = GET_ANSWER_BUTTON_SHOWN;
    qdvc.dxh = @"001005";
    [self presentViewController:qdvc animated:YES completion:nil];

}
```

###### 3 拍照搜索结果显示类SearchViewController 显示"获取答案"按钮

```objective-c    
    CameraViewController *vc = [[CameraViewController alloc] init];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    vc.delegate = self;
    searchViewPageType = GET_ANSWER_BUTTON_SHOWN;
    [self presentViewController:vc animated:YES completion:nil];
}
```

## 验证效果
编译并运行后，可以看到导学号首页如下
<p align="center">
<img src="http://lftcdn.52fdw.com/lftcdn/img/sdk/home.0.0.71.png" width="30%">
</p>

显示“获取答案按钮”
<p align="center">
<img src="http://lftcdn.52fdw.com/lftcdn/img/sdk/showGetAnswerButton.png" width="30%">
</p>

错误码对照表
<p align="center">
<img src="http://lftcdn.52fdw.com/lftcdn/img/sdk/error.png" width="100%">
</p>
## License

DXHSDK is released under the MIT license. See LICENSE for details.
